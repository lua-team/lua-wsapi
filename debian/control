Source: lua-wsapi
Section: interpreters
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper (>= 10~), libfcgi-dev, dh-lua
Standards-Version: 3.9.3
Homepage: http://keplerproject.github.com/wsapi/index.html
Vcs-Git: https://salsa.debian.org/lua-team/lua-wsapi.git
Vcs-Browser: https://salsa.debian.org/lua-team/lua-wsapi

Package: lua-wsapi
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, lua-coxpcall, lua-rings, lua-filesystem, lua-cgi, lua5.1
Pre-Depends: ${misc:Pre-Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Multi-Arch: foreign
Description: Web server API abstraction layer for the Lua language
 WSAPI is an API that abstracts the web server for Lua web applications,
 allowing the same application to be used in different web servers.
 .
 This package contains the WSAPI utility libraries (for common tasks like url
 encoding/decoding) as well as the following backends (server implementations):
 .
  - CGI (the regular environment-variables based protocol)
  - xavante (for the Xavante web server)
 .
 The fastcgi backends is available in the separate package lua-wsapi-fcgi.

Package: lua-wsapi-fcgi
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, lua-wsapi, lua-filesystem, lua5.1
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: Web server API fastcgi backend
 WSAPI is an API that abstracts the web server for Lua web applications,
 allowing the same application to be used in different web servers.
 .
 This package contains the backend for the fastcgi protocol.

Package: lua-wsapi-fcgi-dev
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Section: libdevel
Depends: lua-wsapi-fcgi (= ${binary:Version}), ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: wsapi fastcgi development files for the Lua language
 This package contains the development files of the wsapi library (extra
 backend), useful to create a statically linked binary (like a C application or
 a standalone Lua interpreter).

Package: lua-wsapi-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: wsapi documentation files for the Lua language
 This package contains the documentation of the wsapi library.
